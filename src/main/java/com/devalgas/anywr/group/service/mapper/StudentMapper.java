package com.devalgas.anywr.group.service.mapper;

import com.devalgas.anywr.group.domain.Student;
import com.devalgas.anywr.group.service.dto.StudentDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link Student} and its DTO {@link StudentDTO}.
 *
 * Created by Devalgas
 */
@Mapper(componentModel = "spring")
public interface StudentMapper extends EntityMapper<StudentDTO, Student> {
}
