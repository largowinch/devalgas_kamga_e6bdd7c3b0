package com.devalgas.anywr.group.service;

import org.springframework.security.core.Authentication;

/**
 * Created by Devalgas
 */
public interface TokenService {
     String generateToken(Authentication authentication);
}
