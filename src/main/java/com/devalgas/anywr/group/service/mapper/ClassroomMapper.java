package com.devalgas.anywr.group.service.mapper;

import com.devalgas.anywr.group.domain.Classroom;
import com.devalgas.anywr.group.domain.Teacher;
import com.devalgas.anywr.group.service.dto.ClassroomDTO;
import com.devalgas.anywr.group.service.dto.TeacherDTO;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

/**
 * Mapper for the entity {@link Classroom} and its DTO {@link ClassroomDTO}.
 *
 * Created by Devalgas
 */
@Mapper(componentModel = "spring")
public interface ClassroomMapper extends EntityMapper<ClassroomDTO, Classroom> {
    @Mapping(target = "teacher", source = "teacher", qualifiedByName = "teacherId")
    ClassroomDTO toDto(Classroom s);

    @Named("teacherId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    TeacherDTO toDtoTeacherId(Teacher teacher);
}
