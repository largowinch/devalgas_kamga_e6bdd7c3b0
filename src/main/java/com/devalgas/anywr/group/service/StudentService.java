package com.devalgas.anywr.group.service;

import com.devalgas.anywr.group.domain.Student;
import com.devalgas.anywr.group.service.dto.StudentDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link Student}.
 *
 * Created by Devalgas
 */
public interface StudentService {

    /**
     * Get all the students.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<StudentDTO> findAll(Pageable pageable);

    /**
     * Get all the students with eager load of many-to-many relationships.
     *
     * @param pageable         the pagination information.
     * @param classroomName    the filter information.
     * @param firstNameTeacher the filter information.
     * @param lastNameTeacher  the filter information.
     * @return the list of entities.
     */
    Page<StudentDTO> findByClassroomNameAndTeacherFirstNameContainingAndTeacherLastNameContaining(String classroomName, String firstNameTeacher, String lastNameTeacher, Pageable pageable);
}
