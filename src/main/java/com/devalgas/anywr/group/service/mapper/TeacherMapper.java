package com.devalgas.anywr.group.service.mapper;

import com.devalgas.anywr.group.domain.Teacher;
import com.devalgas.anywr.group.service.dto.TeacherDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link Teacher} and its DTO {@link TeacherDTO}.
 *
 * Created by Devalgas
 */
@Mapper(componentModel = "spring")
public interface TeacherMapper extends EntityMapper<TeacherDTO, Teacher> {
}
