package com.devalgas.anywr.group.service.impl;

import com.devalgas.anywr.group.domain.Student;
import com.devalgas.anywr.group.repository.StudentRepository;
import com.devalgas.anywr.group.service.StudentService;
import com.devalgas.anywr.group.service.dto.StudentDTO;
import com.devalgas.anywr.group.service.mapper.StudentMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Student}.
 *
 * Created by Devalgas
 */
@Service
@Transactional
@Slf4j
public class StudentServiceImpl implements StudentService {
    private final StudentRepository studentRepository;

    private final StudentMapper studentMapper;

    public StudentServiceImpl(StudentRepository studentRepository, StudentMapper studentMapper) {
        this.studentRepository = studentRepository;
        this.studentMapper = studentMapper;
    }

    @Override
    public Page<StudentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Students");
        return studentRepository.findAll(pageable).map(studentMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<StudentDTO> findByClassroomNameAndTeacherFirstNameContainingAndTeacherLastNameContaining(String classroomName, String firstName, String lastName, Pageable pageable) {
        log.debug("Request to get all findByClassroomNameAndTeacherFirstNameContainingAndTeacherLastNameContaining");
        return studentRepository.findByClassroomNameAndTeacherName(classroomName, firstName, lastName, pageable).map(studentMapper::toDto);
    }
}

