package com.devalgas.anywr.group;

import com.devalgas.anywr.group.config.security.RsaKeyProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(RsaKeyProperties.class)
public class DevalgasKamgaE6bdd7c3b0Application {
	public static void main(String[] args) {
		SpringApplication.run(DevalgasKamgaE6bdd7c3b0Application.class, args);
	}
}
