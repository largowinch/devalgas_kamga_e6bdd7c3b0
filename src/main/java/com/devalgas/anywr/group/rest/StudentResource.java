package com.devalgas.anywr.group.rest;

import com.devalgas.anywr.group.service.StudentService;
import com.devalgas.anywr.group.service.dto.StudentDTO;
import com.devalgas.anywr.group.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;

/**
 * REST controller for managing {@link com.devalgas.anywr.group.domain.Student}.
 *
 * Created by Devalgas
 */
@RestController
@RequestMapping("/api")
public class StudentResource {

    private final Logger log = LoggerFactory.getLogger(StudentResource.class);

    private final StudentService studentService;

    public StudentResource(StudentService studentService) {
        this.studentService = studentService;
    }


    /**
     * {@code GET  /students} : get all the students.
     *
     * @param page             the pagination information.
     * @param size             the pagination information.
     * @param classroomName    the filter information.
     * @param firstNameTeacher the filter information.
     * @param lastNameTeacher  the filter information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of students in body.
     */
    @GetMapping("/students")
    public ResponseEntity<List<StudentDTO>> getAllStudents(
            @RequestParam(required = false, defaultValue = "0") int page,
            @RequestParam(required = false, defaultValue = "10") int size,
            @RequestParam(required = false) String classroomName,
            @RequestParam(required = false) String firstNameTeacher,
            @RequestParam(required = false) String lastNameTeacher
    ) {
        log.debug("REST request to get a page of Students classroomName: {}, firstNameTeacher: {}, lastNameTeacher: {}, pageable: {}", classroomName, firstNameTeacher, lastNameTeacher);
        Page<StudentDTO> studentPage;
        PageRequest pageRequest = PageRequest.of(page, size);
        if (classroomName == null && firstNameTeacher == null && lastNameTeacher == null) {
            studentPage = studentService.findAll(pageRequest);
        } else {
            studentPage = studentService.findByClassroomNameAndTeacherFirstNameContainingAndTeacherLastNameContaining(classroomName, firstNameTeacher, lastNameTeacher, pageRequest);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), studentPage);
        return ResponseEntity.ok().headers(headers).body(studentPage.getContent());
    }
}
