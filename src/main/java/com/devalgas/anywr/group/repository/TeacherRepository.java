package com.devalgas.anywr.group.repository;

import com.devalgas.anywr.group.domain.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Teacher entity.
 * <p>
 * Created by Devalgas
 */
@Repository
public interface TeacherRepository extends JpaRepository<Teacher, Long> {
}
