package com.devalgas.anywr.group.repository;

import com.devalgas.anywr.group.domain.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Student entity.
 * <p>
 * Created by Devalgas
 */
@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    @Query("SELECT s FROM Student s JOIN s.classroom c JOIN c.teacher t "
            + "WHERE (:classroomName IS NULL OR c.name LIKE CONCAT('%', :classroomName, '%')) "
            + "AND (:teacherFirstName IS NULL OR t.firstName LIKE CONCAT('%', :teacherFirstName, '%')) "
            + "AND (:teacherLastName IS NULL OR t.lastName LIKE CONCAT('%', :teacherLastName, '%')) "
            + "ORDER BY s.id ASC")
    Page<Student> findByClassroomNameAndTeacherName(@Param("classroomName") String classroomName,
                                                    @Param("teacherFirstName") String teacherFirstName,
                                                    @Param("teacherLastName") String teacherLastName,
                                                    Pageable pageable);
}
