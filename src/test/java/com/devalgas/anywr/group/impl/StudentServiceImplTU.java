package com.devalgas.anywr.group.impl;

import com.devalgas.anywr.group.domain.Classroom;
import com.devalgas.anywr.group.domain.Student;
import com.devalgas.anywr.group.domain.Teacher;
import com.devalgas.anywr.group.repository.StudentRepository;
import com.devalgas.anywr.group.service.dto.StudentDTO;
import com.devalgas.anywr.group.service.impl.StudentServiceImpl;
import com.devalgas.anywr.group.service.mapper.StudentMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class StudentServiceImplTU {
    @Mock
    private StudentRepository studentRepository;

    @Mock
    private StudentMapper studentMapper;

    @InjectMocks
    private StudentServiceImpl studentService;

    @Test
    public void testFindByClassroomNameAndTeacherFirstNameContainingAndTeacherLastNameContaining() {
        List<Student> students = new ArrayList<>();
        Teacher teacher = new Teacher();
        teacher.setId(1L);
        teacher.setFirstName("Devalgas");
        teacher.setLastName("Largowinch");

        Classroom classroom = new Classroom();
        classroom.setId(1L);
        classroom.setName("CM2");
        classroom.setTeacher(teacher);

        Student student1 = new Student();
        student1.setId(1L);
        student1.setFirstName("John");
        student1.setLastName("Doe");
        student1.setClassroom(classroom);
        students.add(student1);
        Page<Student> page = new PageImpl<>(students);

        when(studentRepository.findByClassroomNameAndTeacherName(eq("CM2"), eq("Devalgas"), eq("Largowinch"), any(Pageable.class)))
                .thenReturn(page);
        when(studentMapper.toDto(student1)).thenReturn(new StudentDTO());

        Page<StudentDTO> result = studentService.findByClassroomNameAndTeacherFirstNameContainingAndTeacherLastNameContaining("CM2", "Devalgas", "Largowinch", Pageable.unpaged());

        verify(studentRepository).findByClassroomNameAndTeacherName(eq("CM2"), eq("Devalgas"), eq("Largowinch"), any(Pageable.class));
        verify(studentMapper).toDto(student1);
        assertThat(result).isNotNull();
        assertThat(result.getContent()).hasSize(1);
    }

    @Test
    public void testRetrieveListStudentsCheckElement() {
        List<Student> students = new ArrayList<>();
        Teacher teacher = new Teacher();
        teacher.setId(1L);
        teacher.setFirstName("Devalgas");
        teacher.setLastName("Largowinch");

        Classroom classroom = new Classroom();
        classroom.setId(1L);
        classroom.setName("CM2");
        classroom.setTeacher(teacher);

        Student student1 = new Student();
        student1.setId(1L);
        student1.setFirstName("John");
        student1.setLastName("Doe");
        student1.setClassroom(classroom);
        students.add(student1);
        Page<Student> page = new PageImpl<>(students);

        when(studentRepository.findAll(Pageable.unpaged())).thenReturn(page);
        when(studentMapper.toDto(student1)).thenReturn(new StudentDTO());

        Page<StudentDTO> result = studentService.findAll(Pageable.unpaged());

        verify(studentRepository).findAll(Pageable.unpaged());
        verify(studentMapper).toDto(student1);
        assertThat(result).isNotNull();
        assertThat(result.getContent()).hasSize(1);
    }
}